(function() {



    /**
     * A basic entity class that is composable with simply defined arbitrary components.
     * 
     * Uses (and bundles) the [`uuid`](https://www.npmjs.org/package/uuid) module.
     * 
     * [![Build](http://img.shields.io/travis/lahuan/lahuanjs-ent.svg?style=flat)](https://travis-ci.org/lahuan/lahuanjs-ent)
     * [![Dependencies](http://img.shields.io/david/lahuan/lahuanjs-ent.svg?style=flat)](https://david-dm.org/lahuan/lahuanjs-ent)
     * [![License](http://img.shields.io/badge/license-mit-green.svg?style=flat)](https://github.com/lahuan/lahuanjs-ent/blob/master/LICENSE)
     * [![Release](http://img.shields.io/badge/release-v0.1.3-orange.svg?style=flat)](https://github.com/lahuan/lahuanjs-entrepo/releases)
     * 
     * @module lahuanjs-ent
     * 
     * @alias Ent
     * 
     * @example
     * 
     *  Basic usage:
     * 
     *  ```js
     *  var ent = new Ent();
     * 
     *  var com = function() { this.prop = "val"; };
     *  com.comname = 'com';
     * 
     *  ent.add(com);
     * 
     *  console.log(ent.com.prop);
     *  // val
     *  ```
     * 
     *  An more complex example:
     * 
     *  ```js
     *  
     *  var name = function() {
     *      this._ = "SSS Titanic";
     *  }
     *  name.comname = 'name';
     *  
     *  var hp = function() {
     *      this.max = 100;
     *      this.current = 100;
     *      this.dead = false;
     *  };
     *  hp.prototype.damage = function(value) {
     *      this.current -= value;
     *      if (this.current > this.max) this.current = this.max;
     *      if (this.current <= 0) {
     *          if (this.ent.has(name)) 
     *              console.log(this.ent.name._ + " is destroyed. BOOOOOM!");
     *          this.dead = true;
     *      }
     *  };
     *  hp.comname = 'hp';
     *  
     *  var energy = function() {
     *      this.max = 100;
     *      this.current = 100;
     *  };
     *  energy.prototype.discharge = function(value) {
     *      if (this.current >= value) {
     *          this.current -= value;
     *          return true;
     *      }
     *      else {
     *          return false;
     *      }
     *  };
     *  energy.prototype.recharge = function() {
     *      this.current += 0.1 + this.max / this.current;
     *      if (this.current > this.max) this.current = this.max;
     *  }
     *  energy.comname = 'energy';
     *  
     *  var weapon = function() {
     *      this.damage = 10;
     *      this.heat = 0;
     *      this.cost = 1;
     *  };
     *  weapon.prototype.shoot = function(target) {
     *      var self = this.ent;
     *      var drain = this.cost * (1 + this.heat);
     *      if (self.energy.discharge(drain)) {
     *          if (target.has(hp)) {
     *              target.hp.damage(this.damage);
     *          }
     *          if (self.has(name)) {
     *              if (target.has(name)) {
     *                  console.log(
     *                      self.name._ + " shoots at " + target.name._ + 
     *                      ". Pew pew pew!"
     *                  );
     *              }
     *              else {
     *                  console.log(self.name._ + " shoots something. Zoop zap!");
     *              }
     *          }
     *          this.heat += this.cost/2;
     *      }
     *  };
     *  weapon.prototype.cooldown = function() {
     *      if ( this.heat > 0 ) {
     *          this.heat -= 0.1;
     *          if (this.ent.has(name)) 
     *              console.log(this.ent.name._ + " is cooling its weapons.");
     *      }
     *  };
     *  weapon.prototype.deps = [energy];
     *  weapon.comname = 'weapon';
     *  
     *  var myship = new Ent()
     *      .add([name, hp, energy, weapon])
     *  
     *  var enemyship = new Ent(myship)
     *      .assign({
     *          name: {
     *              _: "Cryobergian Destroyer"
     *          },
     *          weapon: {
     *              damage: 12,
     *              cost: 2
     *          }
     *      });
     *  
     *  while ( !myship.hp.dead && !enemyship.hp.dead ) {
     *  
     *      if ( !myship.hp.dead ) {
     *          myship.energy.recharge();
     *          if ( Math.random() >= 0.5 ) { 
     *              myship.weapon.shoot( enemyship );
     *          } else {
     *              myship.weapon.cooldown();
     *          }
     *      }
     *      
     *      if ( !enemyship.hp.dead ) {
     *          enemyship.energy.recharge();
     *          if ( Math.random() >= 0.5 ) { 
     *              enemyship.weapon.shoot( myship );
     *          } else {
     *              enemyship.weapon.cooldown();
     *          }
     *      }
     *  
     *  }
     * 
     *  // <Log of the epic space duel.>
     *  ```
     * 
     *  Usage in Node:
     *  
     *  ```js
     *      var Ent = require('lahuanjs-ent');
     *      //...
     *  ```
     * 
     *  Usage in the browser via UMD build:
     *  
     *  ```html
     *  <script src="<path>/lahuanjs-ent.js"></script>
     *  <script>
     *  var Ent = lahuanjs.Ent
     *  //...
     *  </script>
     *  ```
     */



    /**
     * Constructs a basic composable entity object to which components can be added.
     * 
     * @param {(module:lahuanjs-ent|string)} [a] An optional argument that can either be another entity whose components and properties will be duplicated OR an UUID string which will be set as the ID for the instance.
     * 
     * @constructor
     * @alias module:lahuanjs-ent
     * 
     * @throws {Error} Will throw an exception if given ID is not an unique UUID.
     */
    var Ent = function( a, b ) {
        
        /**
         * The unique ID for this instance.
         * @readonly
         */
        this.id;
        
        // Reused variables in this scope.
        var id;
        
        // Requires.
        var uuid = require('uuid');

        // If `a` is a string, treat as an UUID.
        if ( typeof a === 'string' ) {
            
            // Parse and validate ID.
            id = uuid.unparse(uuid.parse(a));
            if ( Ent.index[id] !== undefined ) throw new Error( "NonUniqueEntIDError: " + id + " is not an unique UUID for Ent." );
            
        // If `a` is an entity, collect components and properties from it.
        } else if ( a instanceof Ent ) {
            
            // Collect the component instances this has.
            var components = [];
            var ownPropertyNames = Object.getOwnPropertyNames( a );
            for ( var i in ownPropertyNames ) {
                var value = a[ownPropertyNames[i]];
                if ( 
                    value.constructor.comname
                ) {
                    components.push( value.constructor );
                }
            }
            
            // Add components and assign properties as the original.
            if ( components.length > 0 ) {
                this.add ( components );
                this.assign( a );
            }
            
        }
            
        // Generate ID if needed.
        if ( id === undefined ) {
            do {
                id = uuid.v4();
            } while ( Ent.index[id] !== undefined );
        }
        // Set ID.
        Object.defineProperty(this, "id", {
            value: id,
            writable: false
        });
        
        // Index the created instance.
        Ent.index[this.id] = this;
        
    };
    
    /**
     * Adds components to this entity.
     * 
     * Adds in all dependencies of component before adding the component to the entity.
     * Does not overwrite an already existing component.
     * Special initialization method from a component is called if it exists after adding in all dependencies and the entity reference.
     * 
     * @param {(module:lahuanjs-ent~ComLike|module:lahuanjs-ent~ComLike[])} components Either a single component or an array thereof.
     * 
     * @returns Returns itself for chainability.
     */
    Ent.prototype.add = function( components ) {
        
        // If a single component is supplied, wrap it into an array.
        if ( ! ( components instanceof Array ) ) components = [components];
        
        // Recursive function to add a component and its dependencies to an entity.
        var addComponent = function( e, c ) {
            // Don't do anything if component instance already a property.
            if ( e.has( c, true ) ) return;
            // Instantiate `comlike`.
            var instance = new c();
            // Recursively add dependencies before adding component itself.
            if ( instance.deps !== undefined && instance.deps.length > 0 ) {
                for ( var i in instance.deps ) {
                    var d = instance.deps[i];
                    addComponent( e, d );
                }
            }
            // Add the `ent` reference to the `comlike`.
            Object.defineProperty(instance, "ent", {
                value: e,
                writable: false
            });
            // Add the `comlike` to the `ent`.
            e[c.comname] = instance;
            // Call the special init method of the component if it exists.
            var init = instance.init;
            if ( typeof init === 'function' ) init.call(instance);
        };
        //TODO: Either check for circular references or limit recursion.
        
        // Add all component constructors.
        for ( var i in components ) {
            var c = components[i];
            addComponent( this, c );
        }
        
        return this;
    };
    
    /**
     * Removes components from this entity.
     * 
     * Special destruction method from component is called if it exists before removing it from the entity.
     * If no arguments are given then all the added components are removed from this entity and it is itself removed from the index.
     * 
     * Does not do anything if no given component exists on the entity.
     * Does not try to remove any dependencies.
     * 
     * @param {(module:lahuanjs-ent~ComLike|module:lahuanjs-ent~ComLike[])} components Either a single component or an array thereof.
     * 
     * @returns Returns itself for chainability if removing specific components, else nothing.
     */
    Ent.prototype.remove = function( components ) {
    
        var property, destroy;
    
        if ( components !== undefined ) {
        
            // If a single component is supplied, wrap it into an array.
            if ( ! ( components instanceof Array ) ) components = [components];
            
            // Delete all the components specified.
            for ( var i in components ) {
                var c = components[i];
                if ( this.has( c, true ) ) {
                    property = this[c.comname];
                    destroy = property.destroy;
                    if ( typeof destroy === 'function' ) destroy.call(property);
                    delete this[c.comname];
                }
            }
            
            return this;
        
        } else {
            
            // Delete everything.
            var ownPropertyNames = Object.getOwnPropertyNames( this );
            for ( var i in ownPropertyNames ) {
                property = this[ownPropertyNames[i]];
                try {
                    property.destroy.call(property);
                } catch ( e ) {};
                delete this[ownPropertyNames[i]];
            }
            
            // Deindex instance.
            delete Ent.index[this.id];
            
        }
    };
    
    /**
     * Returns whether specific components exist on this entity.
     * 
     * @param {(module:lahuanjs-ent~ComLike|module:lahuanjs-ent~ComLike[])} components Either a single component or an array thereof.
     * @param {boolean} [strict=false] A flag declaring whether the found components are checked to be instances of the respective component constructors.
     * 
     * @throws {Error} If strict mode is true, will throw an exception if a property of the same name already exists but is not an instance of the given component constructor.
     * 
     * @returns Returns true if all the component constructors have instances on this entity, else false.
     */
    Ent.prototype.has = function( components, strict ) {
        
        // If a single component is supplied, wrap it into an array.
        if ( ! ( components instanceof Array ) ) components = [components];
        
        var result = true;
        
        for ( var i in components ) {
            var c = components[i];
            var prop = this[c.comname];
            if ( prop !== undefined ) {
                if ( strict && !( prop instanceof c ) ) {
                    throw new Error( "ComnameMismatchError: " + c.comname + " property occupied by a value that is not an instance of this component constructor." );
                }
            } else {
                result = false;
                break;
            }
        }
        
        return result;
        
    };
    
    /**
     * Assigns arbitrary properties over this entity.
     * 
     * This is similar but _not_ identical to running Lodash `assign` on every object property in the entity.
     * By default, this assigns in all second-level and lower properties of the given object if their first-level objects properties (probably components) exist. Does not alter any of the first-level properties.
     * And if deep mode is set to true then it recursively assigns in only the respective literal properties of every object if their analogs exist, not just the first level ones.
     * 
     * @param {object} properties An arbitrary object.
     * @param {boolean} [deep=false] Whether assignment stops at 
     * 
     * @returns Returns itself for chainability.
     */
    Ent.prototype.assign = function( properties, deep ) {
    
        if ( !deep ) {
            
            var ownPropertyNames = Object.getOwnPropertyNames( properties );
            for ( var i in ownPropertyNames ) {
                
                var ownPropertyName = ownPropertyNames[i];
                var oldComponent = this[ownPropertyName];
                var newComponent = properties[ownPropertyName];
                
                if ( typeof oldComponent === 'object' && typeof newComponent === 'object' ) {
                    
                    var newComponentOwnPropertyNames= Object.getOwnPropertyNames( newComponent );
                    for ( var j in newComponentOwnPropertyNames ) {
                        var newComponentOwnPropertyName = newComponentOwnPropertyNames[j];
                        oldComponent[newComponentOwnPropertyName] = newComponent[newComponentOwnPropertyName];
                    }
                    
                }
                
            }
            
        } else {
            
            var assignDeep = function( oldObject, newObject ) {
                
                if ( typeof oldObject === 'object' && typeof newObject === 'object' ) {
                
                    var ownPropertyNames = Object.getOwnPropertyNames( newObject );
                    for ( var i in ownPropertyNames ) {
                        
                        var ownPropertyName = ownPropertyNames[i];
                        var oldProperty = oldObject[ownPropertyName];
                        var newProperty = newObject[ownPropertyName];
                        
                        if ( oldProperty !== undefined ) {
                            if ( typeof oldProperty === 'object' && typeof newProperty === 'object' ) {
                                assignDeep( oldProperty, newProperty );
                            } else if ( typeof oldProperty !== 'object' && typeof newProperty !== 'object' ) {
                                oldObject[ownPropertyName] = newObject[ownPropertyName];
                            }
                        }
                        
                    }
                    
                }
                
            };
            
            assignDeep( this, properties );
            
        }
    
        return this;
        
    };
    
    /**
     * Chainably calls arbitrary functions under this entity.
     * 
     * Given function will be called without any arguments under the context of this instance.
     * 
     * @param {function} f An arbitrary function.
     * 
     * @returns Returns itself for chainability.
     */
    Ent.prototype.chain = function( f ) {
        f.call(this);
        return this;
    };
    
    
    /**
     * Index of all instantiated Ent entities by their ID.
     */
    Ent.index = Object.create( null );
    
    /**
     * Searches for the entity instances in the index which have the specified components.
     * 
     * @param {(module:lahuanjs-ent~ComLike|module:lahuanjs-ent~ComLike[])} components Either a single component or an array thereof.
     * @param {boolean} [strict=false] A flag declaring whether the found components are checked to be instances of the respective component constructors.
     * 
     * @returns Returns a list of all the Ent instances that have instances of all the given component-constructors.
     */
    Ent.find = function( components, strict ) {
        var result = [];
        for ( var id in Ent.index ) {
            var ent = Ent.index[id];
            if ( ent.has( components, strict ) ) result.push(ent);
        }
        return result;
    };
    /**
     * Alias of {@link module:lahuanjs-ent.find}.
     */
    Ent.$ = Ent.find;
    
    
    
    /**
     * A component constructor: A function that constructs component-like objects, `comlike`s, that can be added to `ent`s.
     * 
     * This is merely a minimum specification for components usable by this module. Any constructor that meets the following can be used as a component constructor.
     * 
     * The constructor function _itself_ have the `comname` property, not the instance.
     * See the specification for a constructed component instances: {@link module:lahuanjs-ent~comlike}.
     * 
     * Being simple JavaScript constructor functions, component constructors can have any prototype chain. Prototypal use of simple component constructors is possible, as long as the final constructor has the `comname` property.
     * 
     * As a matter of convention, it is recommended that component names also be valid JavaScript names (to allow dot traversal) and be in all small letters (not camelCase of PascalCase).
     * 
     * #####Properties
     *  - _comname_ `string` - The component's name.
     * 
     * @typedef {object} ComLike
     * 
     * @property {string} comlike The component's name.
     */
     var ComLike; 
    
    /**
     * A component instance: components constructed by any `ComLike` constructors.
     * 
     * The instance can contain anything, as long as it's actually constructed by its respective component constructor and not just an arbitrary returned object.
     * See the specification for component constructors: {@link module:lahuanjs-ent~ComLike}.
     * 
     * Due to the nature of JavaScript, it is strongly recommended to only put all instance property objects as own properties of comlike objects, and put all methods higher on the prototype chain. 
     * Objects on the prototype chain will be shared by all instances unless overwritten entirely, which may be confusing.
     * And methods not higher on the prototype chain will not only require more memory but also stop components to be easily serializable. They may be first-class objects, but first-class is not enough!
     * Furthermore, it is safe to put literals higher on the prototype chain as well, which may safe some tiny amounts of memory for default values.
     * 
     * Just like any JavaScript object, these component instances can have any prototype chain.
     * 
     * All non-instance properties, those which are meant to be static or shared across all instances, should always go higher on the prototype chain anyway.
     * This includes the `deps`, `init`, and `destroy` special properties.
     * 
     * The following properties within the instance are treated specially.
     * 
     * #####Properties
     *  - _ent_ `Ent` -  Will be overwritten with a read-only reference to the entity object this component belongs to.
     *  - _[deps]_ `ComLike[]` -  A list of components that this component depends on..
     *  - _[init]_ `function` -  Will be invoked after this component initialized to be added to an `ent` object and has its reference.
     *  - _[destroy]_ `function` -  Will be invoked before this component is to be removed from an `ent` object. Note: removal of `comlike`s is not guaranteed to be in any particular order.
     * 
     * @typedef {object} comlike
     * 
     * @property {module:lahuanjs-ent} ent Will be overwritten with a read-only reference to the entity this component belongs to.
     * @property {module:lahuanjs-ent~ComLike[]} [deps] A list of components that this component depends on..
     * @property {function} [init] Will be invoked after this component initialized to be added to an `ent` object and has its reference.
     * @property {function} [destroy] Will be invoked before this component is to be removed from an `ent` object. Note: removal of `comlike`s is not guaranteed to be in any particular order.
     */
     var comlike;
    
    
    
    /*
     * The export object.
     */
    module.exports = Ent;
    
    
    
}).call(this);

//TODO: jsdoc
//TODO: Solve having to manually markup property docs because jsdoc-parse or dmd doesn't support the @property field.
//TODO: Solve having to add redundant variables are because jsdoc-parse or dmd does not understand typedef's are supposed to be used standalone.