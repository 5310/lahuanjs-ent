var test = require( 'tape' ).test;
var Ent = require( '../' );

var components = {};
components.a = function() {
    this.payload = 'a';
};
components.a.comname = 'a';
components.b = function() {
    this.payload = 'b';
    this.deep = { x: 111 };
};
components.b.comname = 'b';
components.c = function() {
    this.deps = [components.a];
    this.init = function() {
        this.ent.a.cInited = true;
    };
    this.destroy = function() {
        if (this.ent.a !== undefined) this.ent.a.cDestroyed = true;
    };
    this.payload = 'c';
};
components.c.comname = 'c';
components.d = function() {
    this.payload = 'd';
};
components.d.comname = 'a';

var assignmentTestRoutine = function( t, ent ) {
    
    // Add components.
    
    ent.add( components.b );
    t.equal( ent.b.payload, 'b', "Component 'b' added." );
    
    ent.add( components.c );
    t.equal( ent.c.payload, 'c', "Component 'c' added." );
    t.equal( ent.a.payload, 'a', "Dependency 'a' of component 'c' also added." );
    t.ok( ent.a.cInited, "Component 'c's special `init` method triggered upon addition." );
    
    var f = function() { ent.add( components.d ); };
    t.throws( f, true, "Trying to add a component by an id which is occupied by a value that is not an instance of that component throws an error." );
    
    // Remove components.
    
    ent.remove( components.c );
    t.ok( ent.a.cDestroyed, "Component 'c's speical `destroy` method triggered upon removal." );
    
    ent.remove( components.a );
    t.ok( ent.a === undefined,  "Component 'a' removed." );
    
    // Check for components.
    
    t.notOk( ent.has( components.a ), "Ent instance does not have component 'a'." );
    
    t.ok( ent.has( components.b, components.c ), "Ent instance does have component 'b'." );
    
    // Assign properties.
    
    ent.assign( { b: { extra: "banana" } } );
    t.equal( ent.b.extra, "banana", "Properties assigned over Ent instance." );
    
    ent.assign( { b: "first level banana" } );
    t.notEqual( ent.b, "first level banana", "Properties not assigned over Ent instance where property not an object which would've overwritten the component." );
    
    
    // console.log( ent );
    ent.assign( { b: { deep: { x: "Sodium", y: "Potassium" } } }, true );
    console.log( ent );
    t.ok( ent.b.deep.x === "Sodium" && ent.b.deep.y === undefined, "Deep properties assigned over Ent instance as expected." );
    
    ent.assign( { e: { extra: "eggplant" } } );
    t.notOk( ent.e, "Properties not assigned over Ent instance where component by the name of the first level property did not exist before." );
    
    // Chain arbitrary functions.
    var r = "";
    var g = function() { r += this.b.extra; };
    ent.chain( g ).chain( g );
    t.equal( r, "bananabanana", "Arbitrary function chaining works." );
    
};

test("Ent constructor tests.", function (t) {
    
    // Plain Ent object.
    
    var entPlain = new Ent();
    assignmentTestRoutine( t, entPlain, "Plain object: " );
    
    // Instantiate by duplicating existing instance.
    
    var entClone = new Ent( entPlain );
    console.log(entClone);
    t.equal( entPlain.b.extra, entClone.b.extra, "Instantiation by duplication works." );
    assignmentTestRoutine( t, entClone );
    
    // Instantiate with ID.
    
    var id = '110ec58a-a0f2-4ac4-8393-c866d813b8d1';
    
    var entByID = new Ent(id);
    t.equal( entByID.id, id, "Instantiation by ID works." );
    assignmentTestRoutine( t, entByID );
    
    var f = function() { var entByID2 = new Ent(id); };
    t.throws( f, true, "Creating Ent instance with non-unique ID throws error." );
    
    // Removal.
    
    id = entPlain.id;
    entPlain.remove();
    t.notOk( Ent.index[id], "Plain Ent instance removed." );
    
    id = entClone.id;
    entClone.remove();
    t.notOk( Ent.index[id], "Duplicated Ent instance removed." );
    
    id = entByID.id;
    entByID.remove();
    t.notOk( Ent.index[id], "Ent instance created with ID removed." );
    
    // Tests done.
    t.end();
    
});

test("Ent index tests.", function (t) {
    
    // Ent objects to find.
    var ent1 = new Ent()
        .add( components.a );
    var ent2 = new Ent()
        .add( [ components.a, components.b ] );
    var ent3 = new Ent()
        .add( [ components.c ] );
    var ent4 = new Ent()
        .add( [ components.a, components.b, components.c ] );
    
    // Find Ent objects by components.
    
    var find1 = Ent.$( components.b );
    t.equal( find1.length, 2, "There are two Ent instances with the component 'b'." );
    
    var find2 = Ent.$( [ components.a ] );
    var r = false;
    for ( var i in find2 ) {
        if ( find2[i] === ent3 ) {
            r = true;
            break;
        }
    }
    t.ok( r, "`ent3` can be found within Ent instances with the 'a' component, despite it not being an explicit add." );
    
    ent4.remove();
    var find4 = Ent.$( [ components.a, components.b, components.c ] );
    t.equal( find4.length, 0, "No single Ent instance with components 'a' 'b' and 'c' can be found after removal of `ent4`.." );
    
    // Remove all.
    
    ent1.remove();
    ent2.remove();
    ent3.remove();
        
    // Tests done.
    t.end();
    
});