<a name="module_lahuanjs-ent"></a>
#lahuanjs-ent
A basic entity class that is composable with simply defined arbitrary components.

Uses (and bundles) the [`uuid`](https://www.npmjs.org/package/uuid) module.

[![Build](http://img.shields.io/travis/lahuan/lahuanjs-ent.svg?style=flat)](https://travis-ci.org/lahuan/lahuanjs-ent)
[![Dependencies](http://img.shields.io/david/lahuan/lahuanjs-ent.svg?style=flat)](https://david-dm.org/lahuan/lahuanjs-ent)
[![License](http://img.shields.io/badge/license-mit-green.svg?style=flat)](https://github.com/lahuan/lahuanjs-ent/blob/master/LICENSE)
[![Release](http://img.shields.io/badge/release-v0.1.3-orange.svg?style=flat)](https://github.com/lahuan/lahuanjs-entrepo/releases)

##Usage

Basic usage:

```js
var ent = new Ent();

var com = function() { this.prop = "val"; };
com.comname = 'com';

ent.add(com);

console.log(ent.com.prop);
// val
```

An more complex example:

```js

var name = function() {
    this._ = "SSS Titanic";
}
name.comname = 'name';

var hp = function() {
    this.max = 100;
    this.current = 100;
    this.dead = false;
};
hp.prototype.damage = function(value) {
    this.current -= value;
    if (this.current > this.max) this.current = this.max;
    if (this.current <= 0) {
        if (this.ent.has(name)) 
            console.log(this.ent.name._ + " is destroyed. BOOOOOM!");
        this.dead = true;
    }
};
hp.comname = 'hp';

var energy = function() {
    this.max = 100;
    this.current = 100;
};
energy.prototype.discharge = function(value) {
    if (this.current >= value) {
        this.current -= value;
        return true;
    }
    else {
        return false;
    }
};
energy.prototype.recharge = function() {
    this.current += 0.1 + this.max / this.current;
    if (this.current > this.max) this.current = this.max;
}
energy.comname = 'energy';

var weapon = function() {
    this.damage = 10;
    this.heat = 0;
    this.cost = 1;
};
weapon.prototype.shoot = function(target) {
    var self = this.ent;
    var drain = this.cost * (1 + this.heat);
    if (self.energy.discharge(drain)) {
        if (target.has(hp)) {
            target.hp.damage(this.damage);
        }
        if (self.has(name)) {
            if (target.has(name)) {
                console.log(
                    self.name._ + " shoots at " + target.name._ + 
                    ". Pew pew pew!"
                );
            }
            else {
                console.log(self.name._ + " shoots something. Zoop zap!");
            }
        }
        this.heat += this.cost/2;
    }
};
weapon.prototype.cooldown = function() {
    if ( this.heat > 0 ) {
        this.heat -= 0.1;
        if (this.ent.has(name)) 
            console.log(this.ent.name._ + " is cooling its weapons.");
    }
};
weapon.prototype.deps = [energy];
weapon.comname = 'weapon';

var myship = new Ent()
    .add([name, hp, energy, weapon])

var enemyship = new Ent(myship)
    .assign({
        name: {
            _: "Cryobergian Destroyer"
        },
        weapon: {
            damage: 12,
            cost: 2
        }
    });

while ( !myship.hp.dead && !enemyship.hp.dead ) {

    if ( !myship.hp.dead ) {
        myship.energy.recharge();
        if ( Math.random() >= 0.5 ) { 
            myship.weapon.shoot( enemyship );
        } else {
            myship.weapon.cooldown();
        }
    }
    
    if ( !enemyship.hp.dead ) {
        enemyship.energy.recharge();
        if ( Math.random() >= 0.5 ) { 
            enemyship.weapon.shoot( myship );
        } else {
            enemyship.weapon.cooldown();
        }
    }

}

// <Log of the epic space duel.>
```

Usage in Node:

```js
    var Ent = require('lahuanjs-ent');
    //...
```

Usage in the browser via UMD build:

```html
<script src="<path>/lahuanjs-ent.js"></script>
<script>
var Ent = lahuanjs.Ent
//...
</script>
```

##API
<a name="exp_module_lahuanjs-ent"></a>
###class: Ent ⏏
**Members**

* [class: Ent ⏏](#exp_module_lahuanjs-ent)
  * [new Ent([a])](#exp_new_module_lahuanjs-ent)
  * [ent.id](#module_lahuanjs-ent#id)
  * [Ent.index](#module_lahuanjs-ent.index)
  * [Ent.$](#module_lahuanjs-ent.$)
  * [ent.add(components)](#module_lahuanjs-ent#add)
  * [ent.remove(components)](#module_lahuanjs-ent#remove)
  * [ent.has(components, [strict])](#module_lahuanjs-ent#has)
  * [ent.assign(properties, [deep])](#module_lahuanjs-ent#assign)
  * [ent.chain(f)](#module_lahuanjs-ent#chain)
  * [Ent.find(components, [strict])](#module_lahuanjs-ent.find)
  * [type: Ent~ComLike](#module_lahuanjs-ent..ComLike)
  * [type: Ent~comlike](#module_lahuanjs-ent..comlike)

<a name="exp_new_module_lahuanjs-ent"></a>
####new Ent([a])
Constructs a basic composable entity object to which components can be added.

#####Params

- _\[a\]_ <code>[lahuanjs-ent](#module_lahuanjs-ent)</code> | `string` - An optional argument that can either be another entity whose components and properties will be duplicated OR an UUID string which will be set as the ID for the instance.  


#####Type

`Error`  

<a name="module_lahuanjs-ent#id"></a>
####ent.id
The unique ID for this instance.

#####Read-only

<a name="module_lahuanjs-ent.index"></a>
####Ent.index
Index of all instantiated Ent entities by their ID.

<a name="module_lahuanjs-ent.$"></a>
####Ent.$
Alias of [find](#module_lahuanjs-ent.find).

<a name="module_lahuanjs-ent#add"></a>
####ent.add(components)
Adds components to this entity.

Adds in all dependencies of component before adding the component to the entity.
Does not overwrite an already existing component.
Special initialization method from a component is called if it exists after adding in all dependencies and the entity reference.

#####Params

- _components_ <code>[ComLike](#module_lahuanjs-ent..ComLike)</code> | <code>[Array.&lt;ComLike&gt;](#module_lahuanjs-ent..ComLike)</code> - Either a single component or an array thereof.  


#####Returns

 - Returns itself for chainability.  

<a name="module_lahuanjs-ent#remove"></a>
####ent.remove(components)
Removes components from this entity.

Special destruction method from component is called if it exists before removing it from the entity.
If no arguments are given then all the added components are removed from this entity and it is itself removed from the index.

Does not do anything if no given component exists on the entity.
Does not try to remove any dependencies.

#####Params

- _components_ <code>[ComLike](#module_lahuanjs-ent..ComLike)</code> | <code>[Array.&lt;ComLike&gt;](#module_lahuanjs-ent..ComLike)</code> - Either a single component or an array thereof.  


#####Returns

 - Returns itself for chainability if removing specific components, else nothing.  

<a name="module_lahuanjs-ent#has"></a>
####ent.has(components, [strict])
Returns whether specific components exist on this entity.

#####Params

- _components_ <code>[ComLike](#module_lahuanjs-ent..ComLike)</code> | <code>[Array.&lt;ComLike&gt;](#module_lahuanjs-ent..ComLike)</code> - Either a single component or an array thereof.  
- _\[strict=false\]_ `boolean` - A flag declaring whether the found components are checked to be instances of the respective component constructors.  


#####Type

`Error`  

#####Returns

 - Returns true if all the component constructors have instances on this entity, else false.  

<a name="module_lahuanjs-ent#assign"></a>
####ent.assign(properties, [deep])
Assigns arbitrary properties over this entity.

This is similar but _not_ identical to running Lodash `assign` on every object property in the entity.
By default, this assigns in all second-level and lower properties of the given object if their first-level objects properties (probably components) exist. Does not alter any of the first-level properties.
And if deep mode is set to true then it recursively assigns in only the respective literal properties of every object if their analogs exist, not just the first level ones.

#####Params

- _properties_ `object` - An arbitrary object.  
- _\[deep=false\]_ `boolean` - Whether assignment stops at  


#####Returns

 - Returns itself for chainability.  

<a name="module_lahuanjs-ent#chain"></a>
####ent.chain(f)
Chainably calls arbitrary functions under this entity.

Given function will be called without any arguments under the context of this instance.

#####Params

- _f_ `function` - An arbitrary function.  


#####Returns

 - Returns itself for chainability.  

<a name="module_lahuanjs-ent.find"></a>
####Ent.find(components, [strict])
Searches for the entity instances in the index which have the specified components.

#####Params

- _components_ <code>[ComLike](#module_lahuanjs-ent..ComLike)</code> | <code>[Array.&lt;ComLike&gt;](#module_lahuanjs-ent..ComLike)</code> - Either a single component or an array thereof.  
- _\[strict=false\]_ `boolean` - A flag declaring whether the found components are checked to be instances of the respective component constructors.  


#####Returns

 - Returns a list of all the Ent instances that have instances of all the given component-constructors.  

<a name="module_lahuanjs-ent..ComLike"></a>
####type: Ent~ComLike
A component constructor: A function that constructs component-like objects, `comlike`s, that can be added to `ent`s.

This is merely a minimum specification for components usable by this module. Any constructor that meets the following can be used as a component constructor.

The constructor function _itself_ have the `comname` property, not the instance.
See the specification for a constructed component instances: [comlike](#module_lahuanjs-ent..comlike).

Being simple JavaScript constructor functions, component constructors can have any prototype chain. Prototypal use of simple component constructors is possible, as long as the final constructor has the `comname` property.

As a matter of convention, it is recommended that component names also be valid JavaScript names (to allow dot traversal) and be in all small letters (not camelCase of PascalCase).

#####Properties
 - _comname_ `string` - The component's name.

#####Scope

inner typedef of [lahuanjs-ent](#module_lahuanjs-ent) 

#####Type

`object`  

<a name="module_lahuanjs-ent..comlike"></a>
####type: Ent~comlike
A component instance: components constructed by any `ComLike` constructors.

The instance can contain anything, as long as it's actually constructed by its respective component constructor and not just an arbitrary returned object.
See the specification for component constructors: [ComLike](#module_lahuanjs-ent..ComLike).

Due to the nature of JavaScript, it is strongly recommended to only put all instance property objects as own properties of comlike objects, and put all methods higher on the prototype chain. 
Objects on the prototype chain will be shared by all instances unless overwritten entirely, which may be confusing.
And methods not higher on the prototype chain will not only require more memory but also stop components to be easily serializable. They may be first-class objects, but first-class is not enough!
Furthermore, it is safe to put literals higher on the prototype chain as well, which may safe some tiny amounts of memory for default values.

Just like any JavaScript object, these component instances can have any prototype chain.

All non-instance properties, those which are meant to be static or shared across all instances, should always go higher on the prototype chain anyway.
This includes the `deps`, `init`, and `destroy` special properties.

The following properties within the instance are treated specially.

#####Properties
 - _ent_ `Ent` -  Will be overwritten with a read-only reference to the entity object this component belongs to.
 - _[deps]_ `ComLike[]` -  A list of components that this component depends on..
 - _[init]_ `function` -  Will be invoked after this component initialized to be added to an `ent` object and has its reference.
 - _[destroy]_ `function` -  Will be invoked before this component is to be removed from an `ent` object. Note: removal of `comlike`s is not guaranteed to be in any particular order.

#####Scope

inner typedef of [lahuanjs-ent](#module_lahuanjs-ent) 

#####Type

`object`  


